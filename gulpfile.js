var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var cssmin = require('gulp-cssmin');
var imagemin = require('gulp-imagemin');
var del = require('del');
var sequence = require('run-sequence');
var babel = require('gulp-babel');

gulp.task('babel', function(){
   return gulp.src('app/js/*.js')
   .pipe(babel({
       presets: ['es2015']
   }))
   .pipe(gulp.dest('app/js2015'))
});

gulp.task('jade', function(){
   return gulp.src('app/index.jade')
   .pipe(jade())
   .pipe(gulp.dest('dist'))
});

gulp.task('sass', function(){
	return gulp.src('app/sass/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('app/css'));
});


gulp.task('watch', function(){
	gulp.watch('app/sass/**/*.scss', ['sass']);
    gulp.watch('app/index.jade', ['jade']);
    gulp.watch('app/js/*.js', ['babel']);
});

gulp.task('useref', function(){
	 return gulp.src('app/*.html')
        .pipe(useref())
		.pipe(gulpif('*.js', uglify()))
		.pipe(gulpif('*.css', cssmin()))
        .pipe(gulp.dest('dist'));
});

gulp.task('imagemin', function(){
	return gulp.src('app/img/*.+(png|jpg|jpeg|gif|svg)')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', function(){
	return gulp.src('app/fonts/**/*.*')
	.pipe(gulp.dest('dist/fonts'));
});

gulp.task('del', function(){
	console.log('Deleting folder')
	return del.sync('dist');
});

gulp.task('build', function(){
	sequence('del', ['useref', 'imagemin', 'fonts']);
	console.log('Building files');
});
