'use strict';
window.onload = function () {
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        if (this.hash) {
            $('body, html').animate({
                scrollTop: $(this.hash).offset().top
            }, 500);
        }
    });
    $(document).on('scroll', function (e) {
        if ($(document).scrollTop() >= $('#slider').offset().top - 100) {
            $('#gotop').show();
        }
        else {
            $('#gotop').hide();
        }
//        if ($(document).scrollTop() + $(window).height() == $(document).height()) {
//            $('#gotop').css('bottom', $('#footer').height());
//        }
//        else{
//            $('#gotop').css('bottom', '');
//        }
    });
    $('#gotop').on('click', function () {
        $('body, html').animate({
            scrollTop: 0
        }, 400);
    });
    let imageHolder = document.getElementById('picholder');
    let bigpic = document.getElementById('bigpic');
    imageHolder.addEventListener("click", function changeImage(event) {
        //event.preventDefault(); 
        if (event.target.src !== undefined) {
            bigpic.src = event.target.src;
        }
    });
    let overlayImage = document.getElementById('overlay-pic');
    let overlay = document.getElementById('overlay');
    let slide = document.getElementById('slide');
    slide.addEventListener("click", function changeOverlayImage(event) {
        if (event.target.src !== undefined && !/.*blank.jpg$/.exec(event.target.src)) {
            overlayImage.src = event.target.src;
            overlay.style.display = "block";
            if (window.innerWidth < 769) {
                document.getElementsByClassName('wrapper')[0].style.display = "none";
            }
        }
    });
    let closeOverlay = document.getElementById("close-overlay");
    closeOverlay.addEventListener("click", function overlayDisplayNone(event) {
        overlay.style.display = "none";
        document.getElementsByClassName('wrapper')[0].removeAttribute('style');
    });
    document.addEventListener("keyup", function overlayDisplayNoneEsc(event) {
        if (event.keyCode === 27) {
            overlay.style.display = "none";
        }
    });
    let imgFolder = 'img/';
    let imgCarousel = [
        {
            title: 'item 1'
            , image: '1.jpg'
        }
    , {
            title: 'item 2'
            , image: '2.jpg'
        }
    , {
            title: 'item 3'
            , image: '3.jpg'
        }
    , {
            title: 'item 4'
            , image: '4.jpg'
        }
    ];
    let slideLeftHandler = document.getElementById('leftArrow');
    let slideRightHandler = document.getElementById('rightArrow');
    let slides = slide.getElementsByTagName('img');
    let captions = slide.getElementsByTagName('figcaption');
    let counterL = 0;

    function generateSlider() {
        let index = counterL;
        if (index > imgCarousel.length - 1) {
            index = 0;
        }
        for (let i = 0; i < slides.length; i++) {
            if (`${imgCarousel[i]}` !== "undefined") {
                slides[i].src = `${imgFolder}${imgCarousel[index].image}`;
                captions[i].innerHTML = `${imgCarousel[index].title}`;
                index++;
                if (index > imgCarousel.length - 1) index = 0;
            }
            else {
                slides[i].src = `${imgFolder}blank.jpg`;
                captions[i].innerHTML = `EMPTY`;
            }
        }
        paginationGenerator();
        paginationActive(counterL);
    }
    slideRightHandler.addEventListener('click', () => {
        if (imgCarousel.length < 1) return;
        counterL++;
        if (counterL > imgCarousel.length - 1) counterL = 0;
        generateSlider();
        console.log(`counterL: ${counterL}`);
    });
    slideLeftHandler.addEventListener('click', () => {
        if (imgCarousel.length < 1) return;
        counterL--;
        if (counterL < 0) counterL = imgCarousel.length - 1;
        generateSlider();
        console.log(`counterL: ${counterL}`);
    });
    let paginationQuantity = Math.ceil(imgCarousel.length / 4);
    let pagination = document.getElementById("pagination");

    function paginationActive(n) {
        for (let i = 0; i < pagination.children.length; i++) {
            pagination.children[i].classList.remove('active');
        }
        let activePos = Math.floor(n / 4);
        pagination.children[activePos].classList.add('active');
    }

    function paginationGenerator() {
        pagination.innerHTML = "";
        paginationQuantity = Math.ceil(imgCarousel.length / 4);
        if (paginationQuantity === 0) paginationQuantity = 1;
        for (let i = 0; i < paginationQuantity; i++) {
            let item = document.createElement('span');
            item.classList.add('item', 'pointer', `page${i}`);
            pagination.appendChild(item);
        }
        pagination.children[0].classList.add('active');
    }
    generateSlider();
    pagination.children[0].classList.add('active');
    pagination.addEventListener('click', (e) => {
        if (e.target.classList.contains('item') && !e.target.classList.contains('active')) {
            let arr = Array.from(pagination.children);
            let page = arr.indexOf(e.target) + 1;
            counterL = 4 * page - 4;
            if (page === arr.length) {
                counterL = 4 * page - 4 - (4 * page - imgCarousel.length);
                console.log(`last page counterL: ${counterL}`);
            }
            for (let i = counterL; i < slides.length + counterL; i++) {
                slides[i - counterL].src = `${imgFolder}${imgCarousel[i].image}`;
                captions[i - counterL].innerHTML = `${imgCarousel[i].title}`;
            }
            paginationActive(counterL + 3);
        }
    });
    let list = [
        {
            id: 1
            , title: 'Item 1'
            , image: '1.jpg'
        }
    , {
            id: 2
            , title: 'Item 2'
            , image: '2.jpg'
        }
    , {
            id: 3
            , title: 'Item 3'
            , image: '3.jpg'
        }
    , {
            id: 4
            , title: 'Item 4'
            , image: '4.jpg'
        }
    , {
            id: 5
            , title: 'Item 5'
            , image: '5.jpg'
        }
    , {
            id: 6
            , title: 'Item 6'
            , image: '6.jpg'
        }
    , {
            id: 7
            , title: 'Item 7'
            , image: '7.jpg'
        }
    , {
            id: 8
            , title: 'Item 8'
            , image: '8.jpg'
        }
    , {
            id: 9
            , title: 'Item 9'
            , image: '9.jpg'
        }
    , {
            id: 10
            , title: 'Item 10'
            , image: '10.jpg'
        }
    , {
            id: 11
            , title: 'Item 11'
            , image: '11.jpg'
        }
    , {
            id: 12
            , title: 'Item 12'
            , image: '12.jpg'
        }
    , {
            id: 13
            , title: 'Item 13'
            , image: '13.jpg'
        }
    , {
            id: 14
            , title: 'Item 14'
            , image: '14.jpg'
        }
    , {
            id: 15
            , title: 'Item 15'
            , image: '15.jpg'
        }
    , {
            id: 16
            , title: 'Item 16'
            , image: '16.jpg'
        }
    , {
            id: 17
            , title: 'Item 17'
            , image: '17.jpg'
        }
    , {
            id: 18
            , title: 'Item 18'
            , image: '18.jpg'
        }
    , {
            id: 19
            , title: 'Item 19'
            , image: '19.jpg'
        }
    , {
            id: 20
            , title: 'Item 20'
            , image: '20.jpg'
        }
    , {
            id: 21
            , title: 'Item 21'
            , image: '21.jpg'
        }
    , {
            id: 22
            , title: 'Item 22'
            , image: '22.jpg'
        }
    , {
            id: 23
            , title: 'Item 23'
            , image: '23.jpg'
        }
    , {
            id: 24
            , title: 'Item 24'
            , image: '24.jpg'
        }
    , {
            id: 25
            , title: 'Item 25'
            , image: '25.jpg'
        }
    , {
            id: 26
            , title: 'Item 26'
            , image: '26.jpg'
        }
    , {
            id: 27
            , title: 'Item 27'
            , image: '27.jpg'
        }
    , {
            id: 28
            , title: 'Item 28'
            , image: '28.jpg'
        }
    , {
            id: 29
            , title: 'Item 29'
            , image: '29.jpg'
        }
    , {
            id: 30
            , title: 'Item 30'
            , image: '30.jpg'
        }
    ];
    let offset = 0;
    let tablePaginator = document.getElementById('tablePaginator');
    let goodsTable = document.getElementById('goodsTable');
    let TablePages = Math.ceil(list.length / 10);
    tablePaginator.addEventListener('click', (e) => {
        if (event.target.tagName !== "SPAN") {
            return;
        }
        //offset = parseInt(e.target.innerHTML)*10 - 10;
        offset = event.target.getAttribute('data-page') * 10 - 10;
        renderTheTable();
    });
    createTablePaginator();
    renderTheTable();

    function createTablePaginator() {
        for (let i = 1; i <= TablePages; i++) tablePaginator.insertAdjacentHTML('beforeend', `<span class="pointer" data-page="${i}">${i}</span>`)
    }

    function renderTheTable() {
        goodsTable.innerHTML = "";
        let items = list.slice(offset, offset + 10);
        items.forEach(item => {
            let tr = document.createElement('tr');
            let td = document.createElement('td');
            td.classList.add('pointer', 'hover', 'title');
            td.innerHTML = item.title;
            let minusTd = document.createElement('td');
            let plusTd = document.createElement('td');
            minusTd.classList.add('pointer', 'minus', 'inactive');
            minusTd.innerHTML = '−';
            minusTd.setAttribute('data-id', `${item.id}`)
            plusTd.classList.add('pointer', 'plus', 'hover');
            plusTd.setAttribute('data-id', `${item.id}`);
            plusTd.innerHTML = '+';
            for (let i = 0; i < imgCarousel.length; i++) {
                if (imgCarousel[i].title === `item ${item.id}`) {
                    plusTd.setAttribute('data-added', true);
                    plusTd.classList.add('inactive');
                    plusTd.classList.remove('hover');
                    minusTd.classList.remove('inactive');
                    minusTd.classList.add('hover');
                }
            }
            tr.appendChild(td);
            tr.appendChild(plusTd);
            tr.appendChild(minusTd);
            goodsTable.appendChild(tr);
        });
    }
    let catalogImage = document.getElementById('catalog-image');
    goodsTable.addEventListener('click', (e) => {
        if (e.target.classList.contains('plus')) {
            if (!e.target.hasAttribute('data-added')) {
                let elem = {
                    title: `item ${e.target.getAttribute('data-id')}`
                    , image: `${e.target.getAttribute('data-id')}.jpg`
                };
                imgCarousel.push(elem);
                generateSlider();
                //            paginationGenerator();
                e.target.setAttribute('data-added', true);
                e.target.classList.add('inactive');
                e.target.classList.remove('hover');
                e.target.nextSibling.classList.remove('inactive');
                e.target.nextSibling.classList.add('hover');
            }
        }
        if (e.target.classList.contains('minus')) {
            if (e.target.previousSibling.hasAttribute('data-added')) {
                let index = 0;
                for (let i = 0; i < imgCarousel.length; i++) {
                    if (imgCarousel[i].title === `item ${e.target.getAttribute('data-id')}`) {
                        imgCarousel.splice(i, 1);
                        index = i;
                    }
                }
                generateSlider();
                e.target.previousSibling.removeAttribute('data-added');
                e.target.classList.add('inactive');
                e.target.classList.remove('hover');
                e.target.previousSibling.classList.remove('inactive');
                e.target.previousSibling.classList.add('hover');
            }
        }
        if (e.target.classList.contains('title')) {
            catalogImage.src = `${imgFolder}${e.target.nextSibling.getAttribute('data-id')}.jpg`
        }
    });

    function httpRequest(url, method, data = null) {
        return new Promise((resolve, resect) => {
            var req = new XMLHttpRequest();
            req.open(method, url, true);
            req.send(data);
            req.onreadystatechange = function () {
                if (req.readyState !== 4) {
                    return;
                }
                if (req.status === 200) {
                    resolve(req.responseText)
                }
                else {
                    reject(`${req.status} ${req.statusText}`)
                }
            }
        });
    };
    let myForm = document.getElementById('contactForm');
    myForm.addEventListener('submit', submitForm);

    function submitForm(event) {
        event.preventDefault();
        let data = {
            name: myForm.name.value
            , email: myForm.email.value
            , topic: myForm.topic.value
            , message: myForm.message.value
        };
        httpRequest('/contact', 'POST', JSON.stringify(data)).then();
    }
    let discountForm = document.getElementById('discountForm');
    let dmy = discountForm.getElementsByClassName('date');
    let submitDiscount = document.getElementById('submitDiscount');
    let countdownDays = document.getElementById('days');
    let countdownHours = document.getElementById('hours');
    let countdownMinutes = document.getElementById('minutes');
    let countdownSeconds = document.getElementById('seconds');
    discountForm.addEventListener('submit', setDiscountCountdown);

    function clearCountDown() {
        countdownDays.children[0].innerHTML = '0';
        countdownDays.children[1].innerHTML = '0';
        countdownHours.children[0].innerHTML = '0';
        countdownHours.children[1].innerHTML = '0';
        countdownMinutes.children[0].innerHTML = '0';
        countdownMinutes.children[1].innerHTML = '0';
        countdownSeconds.children[0].innerHTML = '0';
        countdownSeconds.children[1].innerHTML = '0';
    }

    function setDiscountCountdown(event) {
        event.preventDefault();
        var interval = setInterval(function () {
            let date = new Date();
            if (dmy[0].value === '' || dmy[1].value === '' || dmy[2].value === '') {
                clearCountDown();
                return;
            }
            let date1 = new Date(parseInt(dmy[2].value), parseInt(dmy[1].value) - 1, parseInt(dmy[0].value));
            if (date1 < date) {
                clearCountDown();
                return;
            }
            let daysLeft = Math.floor((date1 - date) / 1000 / 60 / 60 / 24);
            let hoursLeft = Math.floor((date1 - date) / 1000 / 60 / 60) - daysLeft * 24;
            let minutesLeft = Math.floor((date1 - date) / 1000 / 60) - daysLeft * 24 * 60 - hoursLeft * 60;
            let secondsLeft = Math.floor((date1 - date) / 1000) - daysLeft * 24 * 60 * 60 - hoursLeft * 60 * 60 - minutesLeft * 60;
            if (daysLeft > 99) {
                countdownDays.children[0].innerHTML = '9';
                countdownDays.children[1].innerHTML = '9';
            }
            else if (daysLeft < 10) {
                countdownDays.children[0].innerHTML = '0';
                countdownDays.children[1].innerHTML = daysLeft.toString();
            }
            else {
                countdownDays.children[0].innerHTML = daysLeft.toString()[0];
                countdownDays.children[1].innerHTML = daysLeft.toString()[1];
            }
            if (hoursLeft < 10) {
                countdownHours.children[0].innerHTML = '0';
                countdownHours.children[1].innerHTML = hoursLeft.toString();
            }
            else {
                countdownHours.children[0].innerHTML = hoursLeft.toString()[0];
                countdownHours.children[1].innerHTML = hoursLeft.toString()[1];
            }
            if (minutesLeft < 10) {
                countdownMinutes.children[0].innerHTML = '0';
                countdownMinutes.children[1].innerHTML = minutesLeft.toString();
            }
            else {
                countdownMinutes.children[0].innerHTML = minutesLeft.toString()[0];
                countdownMinutes.children[1].innerHTML = minutesLeft.toString()[1];
            }
            if (secondsLeft < 10) {
                countdownSeconds.children[0].innerHTML = '0';
                countdownSeconds.children[1].innerHTML = secondsLeft.toString();
            }
            else {
                countdownSeconds.children[0].innerHTML = secondsLeft.toString()[0];
                countdownSeconds.children[1].innerHTML = secondsLeft.toString()[1];
            }
        }, 1000);
    }
}