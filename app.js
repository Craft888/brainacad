'use strict';

const http = require('http');
const fs = require('fs');
const path = require('path');

let server = http.createServer();

server.on('listening', () => {
   console.log('server is listening on the port 3000');
});


server.on('request', (req, res) => {
    if (req.method ==='POST' && req.url === '/contact'){
        let result = [];
        req
        .on('data', data => result.push(data))
        .on('end', () => {
            result = Buffer.concat(result).toString();
            fs.appendFile('messages.txt', result, (err) => {
               if (!err){
                   res.writeHead(200, {
                      'Set-Cookie': 'form=true' 
                   });
                   res.end(JSON.stringify({result: 'OK'}));
               }
            });
        });
        return
    }
    let filePath = (req.url === '/')? '/dist/index.html' : '/dist'+req.url;
    let contentType = 'text/html';
    let extName = path.extname(req.url);
    switch (extName){
        case '.js':
            contentType = 'application/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.jpg' || '.jpeg':
            contentType = 'image/jpeg';
            break;
        case '.png':
            contentType = 'image/png';
            break;
        case '.gif':
            contentType = 'image/gif';
            break;
    }
    console.log(`${req.method} ${req.url}`);
    fs.readFile(__dirname + filePath, (err, data) => {
        res.setHeader('Content-Type', contentType)
        res.statusCode = 200;
        //res.writeHead(200, {'Content-Type': contentType});
        res.end(data);
    });
    
//    res.write('<h1>Hello!</h1>');
//    res.end(`you requested ${req.url}`);

 });




server.listen(3000);

